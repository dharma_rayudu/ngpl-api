package com.swire.ngpl.service;

import com.swire.ngpl.response.BookingDetailsResponse;

public interface BookingDetailsService {

	public BookingDetailsResponse getAllBookingDetails(String reference);

}
