package com.swire.ngpl.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swire.ngpl.dao.LiveScheduleDao;
import com.swire.ngpl.model.LiveScheduleDetails;
import com.swire.ngpl.response.FunResponse;
import com.swire.ngpl.response.LiveScheduleResponse;
import com.swire.ngpl.response.PortResponse;
import com.swire.ngpl.service.LiveScheduleService;

/**
 * @author Dharma Rayudu
 *
 */
@Service
public class LiveScheduleServiceImpl implements LiveScheduleService {
	private static final Logger log = LoggerFactory.getLogger(LiveScheduleServiceImpl.class);

	@Autowired
	private LiveScheduleDao liveScheduleDao;

	@Override
	public LiveScheduleResponse getAllLiveScheduleDetails(String origin, String destination, String departureFrom,
			String departureTo, String arriveBy) {
		log.info("Inside getAllLiveScheduleDetails(");

		List<LiveScheduleDetails> liveScheduleDetails = liveScheduleDao.getAllLiveScheduleDetails(origin, destination,
				departureFrom, departureTo, arriveBy);

		if (!liveScheduleDetails.isEmpty()) {
			return getLiveScheduleResponse(liveScheduleDetails);

		} else {
			return getLiveScheduleResponse(liveScheduleDetails);
		}

	}

	private LiveScheduleResponse getLiveScheduleResponse(List<LiveScheduleDetails> liveScheduleDetails) {
		log.info("Inside getLiveScheduleResponse()");
		LiveScheduleResponse liveScheduleResponse = new LiveScheduleResponse();

		List<FunResponse> funResponseList = new ArrayList<FunResponse>();
		List<PortResponse> portResposneList = new ArrayList<PortResponse>();

		FunResponse fResponse = new FunResponse();
		PortResponse pResponse = new PortResponse();

		for (LiveScheduleDetails details : liveScheduleDetails) {
			fResponse.setSourceCountry(details.getSourceCountry());
			fResponse.setLeg_type(details.getLeg_type());
			fResponse.setNoOfTransshipMent(details.getNoOfTransshipMent());
			fResponse.setVessel_code(details.getVessel_code());
			fResponse.setDelivery_name(details.getDelivery_name());
			fResponse.setSourcePortName(details.getSourcePortName());
			fResponse.setOrigin(details.getOrigin());
			fResponse.setDisch_callid(details.getDisch_callid());
			fResponse.setFeederInvolved(details.getFeederInvolved());
			fResponse.setLoad_callid(details.getLoad_callid());

			fResponse.setEta(details.getEta());
			fResponse.setEtd(details.getEtd());
			fResponse.setDestinationPortName(details.getDestinationPortName());
			fResponse.setService_code(details.getService_code());
			fResponse.setOrigin_name(details.getOrigin_name());
			fResponse.setVessel_operator(details.getVessel_operator());
			fResponse.setDelivery(details.getDelivery());
			fResponse.setService_name(details.getService_name());
			fResponse.setVessel_imo(details.getVessel_imo());
			fResponse.setBound(details.getBound());

			fResponse.setVoyage_number(details.getVoyage_number());
			fResponse.setVessel_owner_type(details.getVessel_owner_type());
			fResponse.setVessel_name(details.getVessel_name());
			fResponse.setDefault_route(details.getDefault_route());
			fResponse.setDestinationCountry(details.getDestinationCountry());
			fResponse.setDefault_route_order(details.getDefault_route_order());
			fResponse.setInland_flag(details.getInland_flag());
			fResponse.setTransit_time(details.getTransit_time());
			fResponse.setSecondary1(details.getSecondary1());
			fResponse.setVessel_operator_name(details.getVessel_operator_name());
			fResponse.setSecondary2(details.getSecondary2());
			fResponse.setTransshipment_count(details.getTransshipment_count());

			pResponse.setSchedule_type(details.getSchedule_type());
			pResponse.setPod(details.getPod());
			pResponse.setVessel_code(details.getVessel_code());
			pResponse.setService_name(details.getService_name());
			pResponse.setVessel_imo(details.getVessel_imo());
			pResponse.setBound(details.getBound());
			pResponse.setVoyage_number(details.getVoyage_number());
			pResponse.setVessel_owner_type(details.getVessel_owner_type());
			pResponse.setPol(details.getPol());
			pResponse.setVessel_name(details.getVessel_name());
			pResponse.setPol_name(details.getPol_name());
			pResponse.setPod_name(details.getPod_name());

			pResponse.setEta(details.getEta());
			pResponse.setEtd(details.getEtd());
			pResponse.setPol_terminal(details.getPol_terminal());
			pResponse.setPol_terminal_name(details.getPol_terminal_name());
			pResponse.setVessel_operator_name(details.getVessel_operator_name());
			pResponse.setPod_terminal(details.getPod_terminal());
			pResponse.setService_code(details.getService_code());
			pResponse.setPod_terminal_name(details.getPod_terminal_name());
			pResponse.setVessel_operator(details.getVessel_operator());

			portResposneList.add(pResponse);
			funResponseList.add(fResponse);
			fResponse.setPortPair(portResposneList);

		}
		liveScheduleResponse.setFunResponse(funResponseList);
		return liveScheduleResponse;

	}

}
