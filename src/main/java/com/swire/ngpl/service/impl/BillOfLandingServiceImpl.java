package com.swire.ngpl.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swire.ngpl.dao.BillOfLandingDao;
import com.swire.ngpl.model.BookingDetails;
import com.swire.ngpl.model.ContainerActivities;
import com.swire.ngpl.model.ContainerInformation;
import com.swire.ngpl.model.SummaryInformation;
import com.swire.ngpl.response.BookingDetailsResponse;
import com.swire.ngpl.service.BillOfLandingService;

/**
 * @author Dharma Rayudu
 *
 */
@Service
public class BillOfLandingServiceImpl implements BillOfLandingService {
	private static final Logger log = LoggerFactory.getLogger(BillOfLandingServiceImpl.class);

	@Autowired
	private BillOfLandingDao billOfLandingDao;

	@Override
	public BookingDetailsResponse getAllBl(String blNumber) {
		log.info("Inside getAllBl()");

		List<BookingDetails> bookingDetails = billOfLandingDao.getAllForBl(blNumber);

		if (!bookingDetails.isEmpty()) {
			return getBookingDetailsResposne(bookingDetails);
		} else {
			return getBookingDetailsResposne(bookingDetails);
		}

	}

	private BookingDetailsResponse getBookingDetailsResposne(List<BookingDetails> bookingDetails) {
		log.info("Inside getBookingDetailsResposne()");

		BookingDetailsResponse bookingDetailsResponse = new BookingDetailsResponse();
		List<SummaryInformation> sumInfoList = new ArrayList<SummaryInformation>();
		List<ContainerActivities> vvlList = new ArrayList<ContainerActivities>();
		List<ContainerInformation> cnlList = new ArrayList<ContainerInformation>();

		SummaryInformation sumInfo = new SummaryInformation();
		ContainerActivities vvl = new ContainerActivities();
		ContainerInformation cnl = new ContainerInformation();
		String rel = null;

		for (BookingDetails details : bookingDetails) {
			rel = details.getRel();

			sumInfo.setCrg_type("");
			sumInfo.setShp_type("");
			sumInfo.setSch_sts(details.getSch_sts());
			sumInfo.setLst_upd(details.getLst_upd());
			sumInfoList.add(sumInfo);

			vvl.setAct_seq(details.getAct_seq());
			vvl.setAct_sts(details.getAct_sts());
			vvl.setAct_des(details.getAct_des());
			vvl.setAct_dat(details.getAct_dat());
			vvl.setAct_tim(details.getAct_tim());
			vvl.setAct_loc(details.getAct_loc());
			vvl.setAct_vvl(details.getAct_vvl());
			vvlList.add(vvl);

			cnl.setRfrn(details.getRfrn());
			cnl.setLacd(details.getLacd());
			cnl.setLacl(details.getLacl());
			cnl.setLacv(details.getLacv());
			cnl.setGap(details.getGap());
			cnlList.add(cnl);
		}

		bookingDetailsResponse.setRel(rel);
		bookingDetailsResponse.setSum(sumInfoList);
		bookingDetailsResponse.setVvl(vvlList);
		bookingDetailsResponse.setCnl(cnlList);

		return bookingDetailsResponse;
	}

}
