package com.swire.ngpl.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swire.ngpl.dao.ProductDetailsDao;
import com.swire.ngpl.model.ProductData;
import com.swire.ngpl.model.ProductDataDetails;
import com.swire.ngpl.model.ProductDetails;
import com.swire.ngpl.model.SubProducts;
import com.swire.ngpl.response.ProductDetailsResponse;
import com.swire.ngpl.service.ProductDetailsService;

/**
 * @author Dharma Rayudu
 *
 */
@Service
public class ProductDetailsServiceImpl implements ProductDetailsService {
	private static final Logger log = LoggerFactory.getLogger(ProductDetailsServiceImpl.class);

	@Autowired
	private ProductDetailsDao productDetailsDao;

	@Override
	public ProductDetailsResponse getAllProductDetails(String destination, String origin) {
		log.info("Inside getAllProductDetails()");
		List<ProductDetails> detailsList = productDetailsDao.getAllProductDetails(destination, origin);

		if (!detailsList.isEmpty()) {
			return getProductsResponse(detailsList);
		} else {
			return getProductsResponse(detailsList);
		}

	}

	private ProductDetailsResponse getProductsResponse(List<ProductDetails> details) {
		ProductDetailsResponse detailsResponse = new ProductDetailsResponse();
		List<ProductData> pdList = new ArrayList<ProductData>();
		List<SubProducts> subList = new ArrayList<SubProducts>();
		ProductDataDetails data = new ProductDataDetails();
		SubProducts subdata = new SubProducts();
		ProductData pd = new ProductData();

		for (ProductDetails detailsData : details) {

			pd.setSource(detailsData.getSource());
			pd.setDestination(detailsData.getDestination());
			pd.setTransitTimeMin(detailsData.getTransitTimeMin());
			pd.setTransitTimeMax(detailsData.getTransitTimeMax());
			// pd.setNoOfService(detailsData.getNoOfService());
			pd.setTransshipmentMin(detailsData.getTransshipmentMin());
			pd.setTransshipmentMax(detailsData.getTransshipmentMax());
			pd.setNextDepartureDate(detailsData.getNextDepartureDate());
			pd.setDepartureDate(detailsData.getDepartureDate());
			pd.setNextDepartureTime(detailsData.getNextDepartureTime());
			pd.setTranshipmentVariancePdf(detailsData.getTranshipmentVariancePdf());
			pd.setProductDepartureTime(detailsData.getProductDepartureTime());

			subdata.setProductId(detailsData.getProductId());
			subdata.setSourcePort(detailsData.getSourcePort());
			subdata.setSourcePortName(detailsData.getSourcePortName());
			subdata.setDestinationPort(detailsData.getDestinationPort());
			subdata.setDestinationPortName(detailsData.getDestinationPortName());
			subdata.setSourceCity(detailsData.getSourceCity());
			subdata.setDestinationCity(detailsData.getDestinationCity());
			subdata.setSourceCountry(detailsData.getSourceCountry());
			subdata.setDestinationCountry(detailsData.getDestinationCountry());
			subdata.setNoOfTransshipMent(detailsData.getNoOfTransshipMent());
			subdata.setRouteInfo(detailsData.getPld() + "-" + detailsData.getPdc());
			subdata.setGeneralCargo(detailsData.getGeneralCargo());
			subdata.setDangerousCargo(detailsData.getDangerousCargo());
			subdata.setReeferCargo(detailsData.getReeferCargo());
			subdata.setOversizedCargo(detailsData.getOversizedCargo());
			subdata.setContainer_cargo(detailsData.getContainer_cargo());
			subdata.setBreakbulkCargo(detailsData.getBreakbulkCargo());
			subdata.setTransitTimeIndays(detailsData.getTransitTimeIndays());
			subdata.setNextDeparture(detailsData.getNextDeparture());
			subdata.setFrequencyIndays(detailsData.getFrequencyIndays());
			subdata.setShowtoCustomer(detailsData.getShowtoCustomer());
			subdata.setTranshipmentTime(detailsData.getTranshipmentTime());
			subdata.setVarianceFromBest(detailsData.getVarianceFromBest());
			subdata.setTransShipMentPorts(detailsData.getTransShipMentPorts());
			subdata.setFrequency(detailsData.getFrequency() + "Days");
			subdata.setPdftransShipMentPorts(detailsData.getPdftransShipMentPorts());
			subdata.setSubProductDepartureTime(detailsData.getSubProductDepartureTime());
			subdata.setVoyageNo(detailsData.getVoyageNo());
			subdata.setVesselName(detailsData.getVesselName());
			subdata.setCommoditiesNotAllowed(detailsData.getCommoditiesNotAllowed());
			subdata.setFeederInvolved(detailsData.getFeederInvolved());
			subdata.setTransShipMentPortsCodePdf(detailsData.getTransShipMentPortsCodePdf());
			pd.setNoOfService(subList.size());
			pd.setSubProducts(subList);
			data.setData(pdList);
		}

		data.setStatus("SUCCESS");

		detailsResponse.setData(data);
		return detailsResponse;
	}

}
