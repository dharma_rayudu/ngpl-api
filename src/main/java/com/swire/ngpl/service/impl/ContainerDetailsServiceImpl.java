package com.swire.ngpl.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swire.ngpl.dao.ContainerDetailsDao;
import com.swire.ngpl.model.ActivitiesReferenceList;
import com.swire.ngpl.model.BookingDetails;
import com.swire.ngpl.model.ContainerActivities;
import com.swire.ngpl.model.SummaryInformation;
import com.swire.ngpl.response.ContainerDetailsResponse;
import com.swire.ngpl.service.ContainerDetailsService;

/**
 * @author Dharma Rayudu
 *
 */
@Service
public class ContainerDetailsServiceImpl implements ContainerDetailsService {
	private static final Logger log = LoggerFactory.getLogger(ContainerDetailsServiceImpl.class);

	@Autowired
	private ContainerDetailsDao containerDetailsDao;

	@Override
	public ContainerDetailsResponse getAllContainerDetails(String containerNumber) {
		log.info("Inside getAllContainerDetails()");
		List<BookingDetails> containerDetails = containerDetailsDao.getAllContainerDetails(containerNumber);

		if (!containerDetails.isEmpty()) {
			return getContainerresponse(containerDetails);
		} else {
			return getContainerresponse(containerDetails);
		}

	}

	private ContainerDetailsResponse getContainerresponse(List<BookingDetails> containerDetails) {
		log.info("Inside getContainerresponse()");
		ContainerDetailsResponse containerResposne = new ContainerDetailsResponse();
		String rel = null;
		String bl_no = null;
		String bk_no = null;

		List<SummaryInformation> sumInofList = new ArrayList<SummaryInformation>();
		List<ContainerActivities> cacList = new ArrayList<ContainerActivities>();
		List<ActivitiesReferenceList> arlList = new ArrayList<ActivitiesReferenceList>();

		SummaryInformation sumInfo = new SummaryInformation();
		ContainerActivities cac = new ContainerActivities();
		ActivitiesReferenceList arl = new ActivitiesReferenceList();

		for (BookingDetails details : containerDetails) {

			rel = details.getRel();
			bl_no = details.getBl_no();
			bk_no = details.getBk_no();

			sumInfo.setCrg_type("");
			sumInfo.setShp_type("");
			sumInfo.setSch_sts(details.getSch_sts());
			sumInfo.setLst_upd(details.getLst_upd());
			sumInofList.add(sumInfo);

			cac.setAct_seq(details.getAct_seq());
			cac.setAct_sts(details.getAct_sts());
			cac.setAct_des(details.getAct_des());
			cac.setAct_dat(details.getAct_dat());
			cac.setAct_tim(details.getAct_tim());
			cac.setAct_loc(details.getAct_loc());
			cac.setAct_vvl(details.getAct_vvl());
			cac.setLst_upd(details.getLst_upd());
			cacList.add(cac);

			arl.setAct_des(details.getAct_des());
			arl.setAct_loc(details.getAct_loc());
			arl.setAct_fdt(details.getAct_fdt());
			arlList.add(arl);

		}

		containerResposne.setRel(rel);
		containerResposne.setBl_no(bl_no);
		containerResposne.setBk_no(bk_no);

		containerResposne.setSum(sumInofList);
		containerResposne.setCac(cacList);
		containerResposne.setArl(arlList);

		return containerResposne;

	}

}
