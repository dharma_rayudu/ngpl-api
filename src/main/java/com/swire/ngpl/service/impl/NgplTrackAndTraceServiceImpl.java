package com.swire.ngpl.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swire.ngpl.dao.NgplTrackAndTraceDao;
import com.swire.ngpl.model.ActivitiesReferenceList;
import com.swire.ngpl.model.ContainerActivities;
import com.swire.ngpl.response.ContainerAndRefereceListActivitiesResponse;
import com.swire.ngpl.service.NgplTrackAndTraceService;

/**
 * @author Dharma Rayudu
 *
 */
@Service
public class NgplTrackAndTraceServiceImpl implements NgplTrackAndTraceService {

	private static final Logger log = LoggerFactory.getLogger(NgplTrackAndTraceServiceImpl.class);

	@Autowired
	private NgplTrackAndTraceDao ngplTrackAndTraceDao;

	@Override
	public ContainerAndRefereceListActivitiesResponse getContainerTrackingDetails(String reference, String bkno,
			String blno) {
		log.info("Inside getContainerTrackingDetails()");

		List<ContainerActivities> containerData = ngplTrackAndTraceDao.getContainerTrackingDetails(reference, bkno,
				blno);
		List<ActivitiesReferenceList> activitiesData = ngplTrackAndTraceDao.getActivitiesTrackingDetails(reference,
				bkno, blno);

		if (!containerData.isEmpty() && !activitiesData.isEmpty()) {
			return getContainerActivityResponse(containerData, activitiesData);
		} else {
			return getContainerActivityResponse(containerData, activitiesData);
		}

	}

	private ContainerAndRefereceListActivitiesResponse getContainerActivityResponse(
			List<ContainerActivities> containerData, List<ActivitiesReferenceList> activitiesData) {
		log.info("Info getContainerActivityResponse()");

		ContainerAndRefereceListActivitiesResponse containerActivitiesResult = new ContainerAndRefereceListActivitiesResponse();

		containerActivitiesResult.setCact(containerData);
		containerActivitiesResult.setAcrl(activitiesData);
		return containerActivitiesResult;
	}

}
