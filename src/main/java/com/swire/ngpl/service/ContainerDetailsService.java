package com.swire.ngpl.service;

import com.swire.ngpl.response.ContainerDetailsResponse;

public interface ContainerDetailsService {
	
	public ContainerDetailsResponse getAllContainerDetails(String containerNumber);

}
