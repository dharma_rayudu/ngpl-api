package com.swire.ngpl.service;

import com.swire.ngpl.response.ProductDetailsResponse;

/**
 * @author Dharma Rayudu
 *
 */
public interface ProductDetailsService {

	public ProductDetailsResponse getAllProductDetails(String destination, String origin);
}
