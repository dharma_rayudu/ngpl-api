package com.swire.ngpl.service;

import com.swire.ngpl.response.LiveScheduleResponse;

/**
 * @author Dharma Rayudu
 *
 */
public interface LiveScheduleService {
	public LiveScheduleResponse getAllLiveScheduleDetails(String origin, String destination, String departureFrom,
			String departureTo, String arriveBy);

}
