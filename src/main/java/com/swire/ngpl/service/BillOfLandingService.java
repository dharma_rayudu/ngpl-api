package com.swire.ngpl.service;

import com.swire.ngpl.response.BookingDetailsResponse;

public interface BillOfLandingService {

	public BookingDetailsResponse getAllBl(String blNumber);

}
