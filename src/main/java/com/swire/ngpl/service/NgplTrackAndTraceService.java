package com.swire.ngpl.service;

import com.swire.ngpl.response.ContainerAndRefereceListActivitiesResponse;

/**
 * @author Dharma Rayudu
 *
 */
public interface NgplTrackAndTraceService {

	public ContainerAndRefereceListActivitiesResponse getContainerTrackingDetails(String reference, String bkno,
			String blno);

}
