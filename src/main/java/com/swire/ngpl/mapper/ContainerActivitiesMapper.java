package com.swire.ngpl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.swire.ngpl.model.ContainerActivities;

/**
 * @author Dharma Rayudu
 *
 */
public class ContainerActivitiesMapper implements RowMapper<ContainerActivities> {
	private static final Logger log = LoggerFactory.getLogger(ContainerActivitiesMapper.class);

	@Override
	public ContainerActivities mapRow(ResultSet rs, int arg1) throws SQLException {
		log.info("Inside mapRow()");

		ContainerActivities containerData = new ContainerActivities();
		containerData.setAct_seq(rs.getString("inf_seq"));
		containerData.setAct_sts(rs.getString("sts_1"));
		containerData.setAct_des(rs.getString("dsc_1"));
		containerData.setAct_dat(rs.getString("dat_1"));
		containerData.setAct_tim(rs.getString("tim_1"));
		containerData.setAct_loc(rs.getString("inf_1"));
		containerData.setAct_vvl(rs.getString("inf_2"));
		containerData.setLst_upd(rs.getString("dts_1"));

		return containerData;
	}

}
