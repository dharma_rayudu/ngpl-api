package com.swire.ngpl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.swire.ngpl.model.ProductDetails;

/**
 * @author Dharma Rayudu
 *
 */
public class ProductDetailsRowMapper implements RowMapper<ProductDetails> {
	private static final Logger log = LoggerFactory.getLogger(ProductDetailsRowMapper.class);

	@Override
	public ProductDetails mapRow(ResultSet rs, int arg1) throws SQLException {
		log.info("Inside mapRow()");
		
		ProductDetails productData = new ProductDetails();
		productData.setSource(rs.getString("pld_nam"));
		productData.setDestination(rs.getString("pdc_nam"));
		productData.setTransitTimeMin(rs.getInt("trt_tim"));
		productData.setTransitTimeMax(rs.getInt("trt_tim"));
		productData.setTransshipmentMin(0);
		productData.setTransshipmentMax(0);
		productData.setNextDepartureDate("");
		productData.setDepartureDate("");
		productData.setNextDepartureTime("");
		productData.setTranshipmentVariancePdf("");
		productData.setProductDepartureTime(rs.getString("etd"));
		
		
		productData.setProductId("");
		productData.setSourcePort(rs.getString("pld"));
		productData.setSourcePortName(rs.getString("pld_nam"));
		productData.setDestinationPort(rs.getString("pdc"));
		productData.setDestinationPortName(rs.getString("pdc_nam"));
		productData.setSourceCity(rs.getString("pld_nam"));
		productData.setDestinationCity(rs.getString("pdc_nam"));
		productData.setSourceCountry(rs.getString("pld_cnm"));
		productData.setDestinationCountry(rs.getString("pdc_cnm"));
		productData.setNoOfTransshipMent(0);
		productData.setPld(rs.getString("pld"));
		productData.setPdc(rs.getString("pdc"));
		productData.setGeneralCargo("true");
		productData.setDangerousCargo("true");
		productData.setReeferCargo("true");
		productData.setOversizedCargo("true");
		productData.setContainer_cargo("true");
		productData.setBreakbulkCargo("false");
		productData.setTransitTimeIndays(rs.getInt("trt_tim"));
		productData.setNextDeparture("");
		productData.setFrequencyIndays(rs.getInt("frq"));
		productData.setShowtoCustomer("true");
		productData.setTranshipmentTime(0);
		productData.setVarianceFromBest(0);
		productData.setTransShipMentPorts("");
		productData.setFrequency(rs.getString("frq"));
		productData.setPdftransShipMentPorts("");
		productData.setSubProductDepartureTime(rs.getString("etd"));
		productData.setVoyageNo("");
		productData.setVesselName("");
		productData.setCommoditiesNotAllowed("");
		productData.setFeederInvolved("false");
		productData.setTransShipMentPortsCodePdf("");
		
		return productData;
	}

}
