package com.swire.ngpl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.swire.ngpl.model.LiveScheduleDetails;

/**
 * @author Dharma Rayudu
 *
 */
public class LiveScheduleRowMapper implements RowMapper<LiveScheduleDetails> {
	private static final Logger log = LoggerFactory.getLogger(LiveScheduleRowMapper.class);

	@Override
	public LiveScheduleDetails mapRow(ResultSet rs, int arg1) throws SQLException {
		log.info("Inside mapRow()");
		
		LiveScheduleDetails liveScheduleDetails = new LiveScheduleDetails();
		liveScheduleDetails.setSourceCountry(rs.getString("pld_cnm"));
		liveScheduleDetails.setLeg_type("Single");
		liveScheduleDetails.setNoOfTransshipMent("0");
		liveScheduleDetails.setVessel_code(rs.getString("vsl"));
		liveScheduleDetails.setDelivery_name(rs.getString("pdc_nam"));
		liveScheduleDetails.setSourcePortName(rs.getString("pld_nam"));
		liveScheduleDetails.setOrigin(rs.getString("pld"));
		liveScheduleDetails.setDisch_callid("");
		liveScheduleDetails.setFeederInvolved("false");
		liveScheduleDetails.setLoad_callid("");
		liveScheduleDetails.setEta(rs.getString("eta"));
		liveScheduleDetails.setEtd(rs.getString("etd"));
		liveScheduleDetails.setDestinationPortName(rs.getString("pdc_nam"));
		liveScheduleDetails.setService_code(rs.getString("srv"));
		liveScheduleDetails.setOrigin_name(rs.getString("pld_nam"));
		liveScheduleDetails.setVessel_operator("");
		liveScheduleDetails.setDelivery(rs.getString("pdc"));
		liveScheduleDetails.setService_name(rs.getString("srv_nam"));
		liveScheduleDetails.setVessel_imo("");
		liveScheduleDetails.setBound("");
		liveScheduleDetails.setVoyage_number(rs.getString("vyg"));
		liveScheduleDetails.setVessel_owner_type("");
		liveScheduleDetails.setVessel_name(rs.getString("vsl_nam"));
		liveScheduleDetails.setDefault_route("Y");
		liveScheduleDetails.setDestinationCountry(rs.getString("pdc_cnm"));
		liveScheduleDetails.setDefault_route_order("1");
		liveScheduleDetails.setInland_flag("N");
		liveScheduleDetails.setTransit_time(rs.getString("trt_tim"));
		liveScheduleDetails.setSecondary1("N");
		liveScheduleDetails.setVessel_operator_name("");
		liveScheduleDetails.setSecondary2("N");
		liveScheduleDetails.setTransshipment_count("0");
		
		liveScheduleDetails.setSchedule_type("Mainline");
		liveScheduleDetails.setPod(rs.getString("pdc"));
		liveScheduleDetails.setVessel_code(rs.getString("vsl"));
		liveScheduleDetails.setService_name(rs.getString("srv_nam"));
		liveScheduleDetails.setVessel_imo("");
		liveScheduleDetails.setBound("");
		liveScheduleDetails.setVoyage_number(rs.getString("vyg"));
		liveScheduleDetails.setVessel_owner_type("");
		liveScheduleDetails.setPol(rs.getString("pld"));
		liveScheduleDetails.setVessel_name(rs.getString("vsl_nam"));
		liveScheduleDetails.setPol_name(rs.getString("pld_nam"));
		liveScheduleDetails.setPod_name(rs.getString("pdc_nam"));
		liveScheduleDetails.setEta(rs.getString("eta"));
		liveScheduleDetails.setEtd(rs.getString("etd"));
		liveScheduleDetails.setPol_terminal("");
		liveScheduleDetails.setPol_terminal_name("");
		liveScheduleDetails.setVessel_operator_name("");
		liveScheduleDetails.setPod_terminal("");
		liveScheduleDetails.setService_code(rs.getString("srv"));
		liveScheduleDetails.setPod_terminal_name("");
		liveScheduleDetails.setVessel_operator("");
		
		return liveScheduleDetails;
	}

}
