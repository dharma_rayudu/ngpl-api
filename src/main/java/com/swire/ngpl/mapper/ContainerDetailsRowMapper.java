package com.swire.ngpl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.swire.ngpl.model.BookingDetails;

/**
 * @author Dharma Rayudu
 *
 */
public class ContainerDetailsRowMapper implements RowMapper<BookingDetails> {
	private static final Logger log = LoggerFactory.getLogger(ContainerDetailsRowMapper.class);

	@Override
	public BookingDetails mapRow(ResultSet rs, int arg1) throws SQLException {
		log.info("Inside mapRow()");

		BookingDetails bookingDetails = new BookingDetails();

		bookingDetails.setRel(rs.getString("rd_ref"));
		bookingDetails.setSch_sts(rs.getString("sts_1"));
		bookingDetails.setLst_upd(rs.getString("dts_1"));
		bookingDetails.setAct_seq(rs.getString("inf_seq"));
		bookingDetails.setAct_sts(rs.getString("sts_1"));
		bookingDetails.setAct_des(rs.getString("dsc_1"));
		bookingDetails.setAct_dat(rs.getString("dat_1"));
		bookingDetails.setAct_tim(rs.getString("tim_1"));
		bookingDetails.setAct_loc(rs.getString("inf_1"));
		bookingDetails.setAct_vvl(rs.getString("inf_2"));
		bookingDetails.setRfrn(rs.getString("inf_seq"));
		bookingDetails.setLacd(rs.getString("dsc_1"));
		bookingDetails.setLacl(rs.getString("inf_1"));
		bookingDetails.setLacv(rs.getString("inf_2"));
		bookingDetails.setGap(rs.getString("sts_1"));
		bookingDetails.setAct_fdt(rs.getString("inf_seq"));
		bookingDetails.setBl_no(rs.getString("bl_no"));
		bookingDetails.setBk_no(rs.getString("bk_no"));

		return bookingDetails;

	}

}
