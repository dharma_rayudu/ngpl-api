package com.swire.ngpl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.swire.ngpl.model.ActivitiesReferenceList;

/**
 * @author Dharma Rayudu
 *
 */
public class ActivitiesReferenceMapper implements RowMapper<ActivitiesReferenceList> {
	private static final Logger log = LoggerFactory.getLogger(ActivitiesReferenceMapper.class);

	@Override
	public ActivitiesReferenceList mapRow(ResultSet rs, int arg1) throws SQLException {
		log.info("Inside mapRow()");

		ActivitiesReferenceList activitiesData = new ActivitiesReferenceList();
		activitiesData.setAct_des(rs.getString("dsc_1"));
		activitiesData.setAct_loc(rs.getString("inf_1"));
		activitiesData.setAct_fdt(rs.getString("inf_seq"));

		return activitiesData;
	}

}
