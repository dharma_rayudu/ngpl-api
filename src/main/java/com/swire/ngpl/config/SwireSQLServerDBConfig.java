package com.swire.ngpl.config;

import java.io.IOException;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ComponentScan("com.swire.ngpl.config")
// @PropertySource("classpath:database.properties")
// @ConfigurationProperties("spring.datasource")
public class SwireSQLServerDBConfig {

	@Autowired
	Environment environment;

	private final String URL = "sqlserver.url";
	private final String USER = "sqlserver.dbuser";
	private final String DRIVER = "sqlserver.driver";
	private final String PASSWORD = "sqlserver.dbpassword";
	private static final Logger LOGGER = LogManager.getLogger(SwireSQLServerDBConfig.class.getName());

	@Bean(name = "sqlserver")
	@Profile("default")
	DataSource dataSource() {
		LOGGER.info("########### DataSource in default profile ############");
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUrl(environment.getProperty(URL));
		driverManagerDataSource.setUsername(environment.getProperty(USER));
		driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
		driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
		return driverManagerDataSource;
	}

	/*@Bean(name = "sqlserver")
	@Profile("dev")
	DataSource devDataSource() {
		LOGGER.info("########### DataSource in dev profile ############");
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUrl(environment.getProperty(URL));
		driverManagerDataSource.setUsername(environment.getProperty(USER));
		driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
		driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
		return driverManagerDataSource;
	}*/
	
	@SuppressWarnings("unchecked")
	@Bean(name = "sqlserver")
	@Profile("dev")
	DataSource devDataSource() {
		LOGGER.info("########### DataSource in dev profile ############");
		String secretName = "swire.dev.ngplapi.sqlserver.db.credentials";
		LOGGER.info("secretName =" + secretName);

		ObjectMapper mapper = new ObjectMapper();
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();

		try {
			String secret = SecretManager.getSecret(secretName);
			Map<String, String> map = mapper.readValue(secret, Map.class);

			String url = map.get("url");
			LOGGER.info("url=" + url);
			String username = map.get("username");
			String password = map.get("password");
			String driver = map.get("driver");

			driverManagerDataSource.setUrl(url);
			driverManagerDataSource.setUsername(username);
			driverManagerDataSource.setPassword(password);
			driverManagerDataSource.setDriverClassName(driver);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return driverManagerDataSource;
	}

	@SuppressWarnings("unchecked")
	@Bean(name = "sqlserver")
	@Profile("stage")
	DataSource stageDataSource() {
		LOGGER.info("########### DataSource in stage profile  ############");

		String secretName = "swire.stage.ngplapi.sqlserver.db.credentials";
		LOGGER.info("secretName =" + secretName);

		
		ObjectMapper mapper = new ObjectMapper();
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();

		try {
			String secret = SecretManager.getSecret(secretName);
			Map<String, String> map = mapper.readValue(secret, Map.class);

			String url = map.get("url");
			LOGGER.info("url=" + url);
			String username = map.get("username");
			String password = map.get("password");
			String driver = map.get("driver");

			driverManagerDataSource.setUrl(url);
			driverManagerDataSource.setUsername(username);
			driverManagerDataSource.setPassword(password);
			driverManagerDataSource.setDriverClassName(driver);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		
		/*DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUrl(environment.getProperty(URL));
		driverManagerDataSource.setUsername(environment.getProperty(USER));
		driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
		driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));*/
		return driverManagerDataSource;
	}

	@SuppressWarnings("unchecked")
	@Bean(name = "sqlserver")
	@Profile("prod")
	DataSource prodDataSource() {
		LOGGER.info("########### DataSource in prod profile ############");
		String secretName = "swire.dev.ngplapi.sqlserver.db.credentials";
		LOGGER.info("secretName =" + secretName);

		
		ObjectMapper mapper = new ObjectMapper();
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();

		try {
			String secret = SecretManager.getSecret(secretName);
			Map<String, String> map = mapper.readValue(secret, Map.class);

			String url = map.get("url");
			LOGGER.info("url=" + url);
			String username = map.get("username");
			String password = map.get("password");
			String driver = map.get("driver");

			driverManagerDataSource.setUrl(url);
			driverManagerDataSource.setUsername(username);
			driverManagerDataSource.setPassword(password);
			driverManagerDataSource.setDriverClassName(driver);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		/*
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUrl(environment.getProperty(URL));
		driverManagerDataSource.setUsername(environment.getProperty(USER));
		driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
		driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));*/
		return driverManagerDataSource;
	}


	@Bean(name = "sqlserverJdbcTemplate")
	public JdbcTemplate jdbcTemplate(DataSource sqlserver) {
		return new JdbcTemplate(sqlserver);
	}

	@Bean(name = "sqlserverJdbcTemplate")
	public NamedParameterJdbcTemplate NamedParameterJdbcTemplate(DataSource sqlserver) {
		return new NamedParameterJdbcTemplate(sqlserver);
	}

}
