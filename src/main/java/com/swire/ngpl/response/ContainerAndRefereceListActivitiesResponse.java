package com.swire.ngpl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swire.ngpl.model.ActivitiesReferenceList;
import com.swire.ngpl.model.ContainerActivities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder({"CACT", "ACRL"})
public class ContainerAndRefereceListActivitiesResponse {

	@JsonProperty("CACT")
	private List<ContainerActivities> cact;

	@JsonProperty("ACRL")
	private List<ActivitiesReferenceList> acrl;

}
