package com.swire.ngpl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swire.ngpl.model.ActivitiesReferenceList;
import com.swire.ngpl.model.ContainerActivities;
import com.swire.ngpl.model.SummaryInformation;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Dharma Rayudu
 *
 */
@Getter
@Setter
@JsonPropertyOrder({"REL", "BL_NO", "BK_NO", "SUM", "CAC", "ARL"})
public class ContainerDetailsResponse {
	
	@JsonProperty("REL")
	private String rel;

	@JsonProperty("BL_NO")
	private String bl_no;

	@JsonProperty("BK_NO")
	private String bk_no;

	@JsonProperty("SUM")
	private List<SummaryInformation> sum;

	@JsonProperty("CAC")
	private List<ContainerActivities> cac;

	@JsonProperty("ARL")
	private List<ActivitiesReferenceList> arl;

}
