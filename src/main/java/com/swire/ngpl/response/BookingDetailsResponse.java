package com.swire.ngpl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swire.ngpl.model.ContainerActivities;
import com.swire.ngpl.model.ContainerInformation;
import com.swire.ngpl.model.SummaryInformation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder({"REL", "SUM", "VVL", "CNL"})
public class BookingDetailsResponse {

	@JsonProperty("REL")
	private String rel;

	@JsonProperty("SUM")
	private List<SummaryInformation> sum;

	@JsonProperty("VVL")
	private List<ContainerActivities> vvl;

	@JsonProperty("CNL")
	private List<ContainerInformation> cnl;

}
