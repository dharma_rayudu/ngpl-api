package com.swire.ngpl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Dharma Rayudu
 *
 */
@Getter
@Setter
public class FunResponse{
	
	@JsonProperty("SOURCECOUNTRY")
	private String sourceCountry;
	
	@JsonProperty("LEG_TYPE")
	private String leg_type;
	
	@JsonProperty("NOOFTRANSSHIPMENT")
	private String noOfTransshipMent;
	
	@JsonProperty("VESSEL_CODE")
	private String vessel_code;
	
	@JsonProperty("DELIVERY_NAME")
	private String delivery_name;
	
	@JsonProperty("SOURCEPORTNAME")
	private String sourcePortName;
	
	@JsonProperty("ORIGIN")
	private String origin;
	
	@JsonProperty("DISCH_CALLID")
	private String disch_callid;
	
	@JsonProperty("FEEDERINVOLVED")
	private String feederInvolved;
	
	@JsonProperty("LOAD_CALLID")
	private String load_callid;
	
	@JsonProperty("ETA")
	private String eta;
	
	@JsonProperty("ETD")
	private String etd;
	
	@JsonProperty("DESTINATIONPORTNAME")
	private String destinationPortName;
	
	@JsonProperty("SERVICE_CODE")
	private String service_code;
	
	@JsonProperty("ORIGIN_NAME")
	private String origin_name;
	
	@JsonProperty("VESSEL_OPERATOR")
	private String vessel_operator;
	
	@JsonProperty("DELIVERY")
	private String delivery;
	
	@JsonProperty("SERVICE_NAME")
	private String service_name;
	
	@JsonProperty("VESSEL_IMO")
	private String vessel_imo;
	
	@JsonProperty("BOUND")
	private String bound;
	
	@JsonProperty("VOYAGE_NUMBER")
	private String voyage_number;
	
	@JsonProperty("VESSEL_OWNER_TYPE")
	private String vessel_owner_type;
	
	@JsonProperty("VESSEL_NAME")
	private String vessel_name;
	
	@JsonProperty("DEFAULT_ROUTE")
	private String default_route;
	
	@JsonProperty("DESTINATIONCOUNTRY")
	private String destinationCountry;
	
	@JsonProperty("DEFAULT_ROUTE_ORDER")
	private String default_route_order;
	
	@JsonProperty("INLAND_FLAG")
	private String inland_flag;
	
	@JsonProperty("TRANSIT_TIME")
	private String transit_time;
	
	@JsonProperty("SECONDARY1")
	private String secondary1;
	
	@JsonProperty("VESSEL_OPERATOR_NAME")
	private String vessel_operator_name;
	
	@JsonProperty("SECONDARY2")
	private String secondary2;
	
	@JsonProperty("TRANSSHIPMENT_COUNT")
	private String transshipment_count;
	
	@JsonProperty("PORTPAIR")
	private List<PortResponse> portPair;

}
