package com.swire.ngpl.response;

import com.swire.ngpl.model.ProductDataDetails;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDetailsResponse {
	
	private ProductDataDetails data;

}
