package com.swire.ngpl.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Dharma Rayudu
 *
 */
@Getter
@Setter
public class LiveScheduleResponse{
	
	@JsonProperty("FUNRESPONSE")
	private List<FunResponse> funResponse;

}
