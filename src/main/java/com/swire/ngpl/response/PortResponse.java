package com.swire.ngpl.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Dharma Rayudu
 *
 */
@Getter
@Setter
public class PortResponse{

	@JsonProperty("SCHEDULE_TYPE")
	private String schedule_type;
	
	@JsonProperty("POD")
	private String pod;
	
	@JsonProperty("VESSEL_CODE")
	private String vessel_code;
	
	@JsonProperty("SERVICE_NAME")
	private String service_name;
	
	@JsonProperty("VESSEL_IMO")
	private String vessel_imo;
	
	@JsonProperty("BOUND")
	private String bound;
	
	@JsonProperty("VOYAGE_NUMBER")
	private String voyage_number;
	
	@JsonProperty("VESSEL_OWNER_TYPE")
	private String vessel_owner_type;
	
	@JsonProperty("POL")
	private String pol;
	
	@JsonProperty("VESSEL_NAME")
	private String vessel_name;
	
	@JsonProperty("POL_NAME")
	private String pol_name;
	
	@JsonProperty("POD_NAME")
	private String pod_name;
	
	@JsonProperty("ETA")
	private String eta;
	
	@JsonProperty("ETD")
	private String etd;
	
	@JsonProperty("POL_TERMINAL")
	private String pol_terminal;
	
	@JsonProperty("POL_TERMINAL_NAME")
	private String pol_terminal_name;
	
	@JsonProperty("VESSEL_OPERATOR_NAME")
	private String vessel_operator_name;
	
	@JsonProperty("POD_TERMINAL")
	private String pod_terminal;
	
	@JsonProperty("SERVICE_CODE")
	private String service_code;
	
	@JsonProperty("POD_TERMINAL_NAME")
	private String pod_terminal_name;
	
	@JsonProperty("VESSEL_OPERATOR")
	private String vessel_operator;
}
