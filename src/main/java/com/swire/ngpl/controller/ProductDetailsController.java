package com.swire.ngpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swire.ngpl.response.ProductDetailsResponse;
import com.swire.ngpl.service.ProductDetailsService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/swire-ngpl/api/vi/Product")
public class ProductDetailsController {
	private static final Logger log = LoggerFactory.getLogger(ProductDetailsController.class);

	@Autowired
	private ProductDetailsService productDetailsService;

	@GetMapping(value = "/getProductsDetail")
	@ApiOperation(value = "Get All Details for Product Search", notes = "To get all the details for a set of Origin and Destination")
	public ProductDetailsResponse getAllProductDetails(@RequestParam String destinationName,
			@RequestParam(defaultValue = "Ports", required = false) String destinationType,
			@RequestParam String sourceName, @RequestParam(value = "Country", required = false) String sourceType) {
		log.info("Inside getAllProductDetails()");

		return productDetailsService.getAllProductDetails(destinationName, sourceName);
	}

}
