package com.swire.ngpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swire.ngpl.response.ContainerDetailsResponse;
import com.swire.ngpl.service.ContainerDetailsService;

import io.swagger.annotations.ApiOperation;

/**
 * @author Dharma Rayudu
 *
 */
@RestController
@RequestMapping(value = "/swire-ngpl/api/vi/TrackTrace")
public class ContainerDetailsController {
	private static final Logger log = LoggerFactory.getLogger(ContainerDetailsController.class);

	@Autowired
	private ContainerDetailsService containerDetails;

	@GetMapping(value = "/getAllForContainer/{containerNumber}")
	@ApiOperation(value = "Get All Details for Container(s)", notes = "To get all the details for a single Container Number")
	public ContainerDetailsResponse getAllContainers(@PathVariable String containerNumber) {
		log.info("Inside getAllContainers()");

		return containerDetails.getAllContainerDetails(containerNumber);
	}
}
