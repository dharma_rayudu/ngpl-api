package com.swire.ngpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swire.ngpl.response.BookingDetailsResponse;
import com.swire.ngpl.service.BookingDetailsService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/swire-ngpl/api/vi/TrackTrace")
public class BookingDetailsController {

	private static final Logger log = LoggerFactory.getLogger(BookingDetailsController.class);

	@Autowired
	private BookingDetailsService bookingDetailsService;

	@GetMapping(value = "/getAllForBooking/{reference}")
	@ApiOperation(value = "Get All Details for Booking Number", notes = "To get all the details for a single Booking Number")
	public BookingDetailsResponse getAllForBooking(@PathVariable String reference) {
		log.info("Inside getAllForBooking()");

		return bookingDetailsService.getAllBookingDetails(reference);
	}

}
