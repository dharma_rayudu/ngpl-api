package com.swire.ngpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swire.ngpl.response.ContainerAndRefereceListActivitiesResponse;
import com.swire.ngpl.service.NgplTrackAndTraceService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/swire-ngpl/api/vi/TrackTrace")
public class NgplTrackAndTraceController {
	private static final Logger log = LoggerFactory.getLogger(NgplTrackAndTraceController.class);

	@Autowired
	private NgplTrackAndTraceService ngplTrackAndTraceService;

	@GetMapping(value = "/getTrackingForContainer")
	@ApiOperation(value = "Get Tracking Information for Container", notes = "Get Tracking Information for Container Number, along with corresponding Booking Number or BL Number (if linked)")
	public ContainerAndRefereceListActivitiesResponse getTrackingInfoByContainer(@RequestParam String reference,
			@RequestParam String bkno, @RequestParam String blno) {

		log.info("Inside getTrackingInfoByContainer()");

		return ngplTrackAndTraceService.getContainerTrackingDetails(reference, bkno, blno);

	}

}
