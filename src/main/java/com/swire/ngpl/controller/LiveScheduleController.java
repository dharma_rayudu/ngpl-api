package com.swire.ngpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swire.ngpl.response.LiveScheduleResponse;
import com.swire.ngpl.service.LiveScheduleService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/swire-ngpl/api/vi/Schedule")
public class LiveScheduleController {
	private static final Logger log = LoggerFactory.getLogger(LiveScheduleController.class);

	@Autowired
	private LiveScheduleService liveScheduleService;

	@RequestMapping(value = "/getLiveSchedule")
	@ApiOperation(value = "Get All Details for Live Schedules", notes = "To get all the live schedule details for a set of Origin, Destination, Range of Estimated Time of Departure(ETD) and Estimated Time of Arrival(ETA)")
	public LiveScheduleResponse getAllLiveScheduleDetails(@RequestParam String origin, @RequestParam String destination,
			@RequestParam String departureFrom, @RequestParam String departureTo, @RequestParam String arriveBy) {
		log.info("Inside getAllLiveScheduleDetails()");

		return liveScheduleService.getAllLiveScheduleDetails(origin, destination, departureFrom, departureTo, arriveBy);
	}
}
