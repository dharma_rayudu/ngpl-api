package com.swire.ngpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swire.ngpl.response.BookingDetailsResponse;
import com.swire.ngpl.service.BillOfLandingService;

import io.swagger.annotations.ApiOperation;

/**
 * @author Dharma Rayudu
 *
 */
@RestController
@RequestMapping(value = "/swire-ngpl/api/vi/TrackTrace")
public class BillOfLandingController {
	private static final Logger log = LoggerFactory.getLogger(BillOfLandingController.class);

	@Autowired
	private BillOfLandingService billOfLandingService;

	@GetMapping(value = "/getAllForBL/{blNumber}")
	@ApiOperation(value = "Get All Details for BL", notes = "To get all the details for a single BL Number")
	public BookingDetailsResponse getAllBl(@PathVariable String blNumber) {

		log.info("Inside getAllBl");
		return billOfLandingService.getAllBl(blNumber);
	}

}
