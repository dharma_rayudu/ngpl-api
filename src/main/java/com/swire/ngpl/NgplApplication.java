package com.swire.ngpl;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author Dharma Rayudu
 *
 */
@SpringBootApplication
public class NgplApplication {

	public static void main(String[] args) {
		SpringApplication.run(NgplApplication.class, args);
	}

}
