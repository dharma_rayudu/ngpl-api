package com.swire.ngpl.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductData{
	
	@JsonProperty("SOURCE")
	private String source;
	
	@JsonProperty("DESTINATION")
	private String destination;
	
	@JsonProperty("TRANSITTIMEMIN")
	private int transitTimeMin;
	
	@JsonProperty("TRANSITTIMEMAX")
	private int transitTimeMax;
	
	@JsonProperty("ONOFSERVICE")
	private int noOfService;
	
	@JsonProperty("TRANSSHIPMENTMIN")
	private int transshipmentMin;
	
	@JsonProperty("TRANSSHIPMENTMAX")
	private int transshipmentMax;
	
	@JsonProperty("NEXTDEPARTUREDATE")
	private String nextDepartureDate;
	
	@JsonProperty("DEPARTUREDATE")
	private String departureDate;
	
	@JsonProperty("NEXTDEPARTURETIME")
	private String nextDepartureTime;
	
	@JsonProperty("TRANSHIPMENTVARIANCEPDF")
	private String transhipmentVariancePdf;
	
	@JsonProperty("PRODUCTDEPARTURETIME")
	private String productDepartureTime;

	@JsonProperty("SUBPRODUCTS")
	private List<SubProducts> subProducts; 
}
