package com.swire.ngpl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubProducts{
	
	@JsonProperty("PRODUCTID")
	private String productId;
	
	@JsonProperty("SOURCEPORT")
	private String sourcePort;
	
	@JsonProperty("SOURCEPORTNAME")
	private String sourcePortName;
	
	@JsonProperty("DESTINATIONPORT")
	private String destinationPort;
	
	@JsonProperty("DESTINATIONPORTNAME")
	private String destinationPortName;
	
	@JsonProperty("SOURCECITY")
	private String sourceCity;
	
	@JsonProperty("DESTINATIONCITY")
	private String destinationCity;
	
	@JsonProperty("SOURCECOUNTRY")
	private String sourceCountry;
	
	@JsonProperty("DESTINATIONCOUNTRY")
	private String destinationCountry;
	
	@JsonProperty("NOOFTRANSSHIPMENT")
	private int noOfTransshipMent;
	
	@JsonProperty("ROUTEINFO")
	private String routeInfo;
	
	@JsonProperty("GENERALCARGO")
	private String generalCargo;
	
	@JsonProperty("DANGEROUSCARGO")
	private String dangerousCargo;
	
	@JsonProperty("REEFERCARGO")
	private String reeferCargo;
	
	@JsonProperty("OVERSIZEDCARGO")
	private String oversizedCargo;
	
	@JsonProperty("CONTAINER_CARGO")
	private String container_cargo;
	
	@JsonProperty("BREAKBULKCARGO")
	private String breakbulkCargo;
	
	@JsonProperty("TRANSITTIMEINDAYS")
	private int transitTimeIndays;
	
	@JsonProperty("NEXTDEPARTURE")
	private String nextDeparture;
	
	@JsonProperty("FREQUENCYINDAYS")
	private int frequencyIndays;
	
	@JsonProperty("SHOWTOCUSTOMER")
	private String showtoCustomer;
	
	@JsonProperty("TRANSHIPMENTTIME")
	private int transhipmentTime;
	
	@JsonProperty("VARIANCEFROMBEST")
	private int varianceFromBest;
	
	@JsonProperty("TRANSSHIPMENTPORTS")
	private String transShipMentPorts;
	
	@JsonProperty("FREQUENCY")
	private String frequency;
	
	@JsonProperty("PDFTRANSSHIPMENTPORTS")
	private String pdftransShipMentPorts;
	
	@JsonProperty("SUBPRODUCTDEPARTURETIME")
	private String subProductDepartureTime;
	
	@JsonProperty("VOYAGENO")
	private String voyageNo;
	
	@JsonProperty("VESSELNAME")
	private String vesselName;
	
	@JsonProperty("COMMODITIESNOTALLOWED")
	private String commoditiesNotAllowed;
	
	@JsonProperty("FEEDERINVOLVED")
	private String feederInvolved;
	
	@JsonProperty("TRANSSHIPMENTPORTSCODEPDF")
	private String transShipMentPortsCodePdf;

}
