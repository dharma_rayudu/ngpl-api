package com.swire.ngpl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContainerInformation{

	@JsonProperty("RFRN")
	private String rfrn;
	
	@JsonProperty("LACD")
	private String lacd;
	
	@JsonProperty("LACL")
	private String lacl;

	@JsonProperty("LACV")
	private String lacv;

	@JsonProperty("GAP")
	private String gap;
}
