package com.swire.ngpl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * @author Dharma Rayudu
 *
 */
@Data
public class ActivitiesReferenceList {

	@JsonProperty("ACT_DES")
	private String act_des;

	@JsonProperty("ACT_LOC")
	private String act_loc;

	@JsonProperty("ACT_FDT")
	private String act_fdt;
}
