package com.swire.ngpl.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDataDetails{

	@JsonProperty("STATUS")
	private String status;
	
	@JsonProperty("DATA")
	private List<ProductData> data;
}
