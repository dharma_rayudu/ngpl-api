package com.swire.ngpl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SummaryInformation {
	
	@JsonProperty("CRG_TYPE")
	private String crg_type;
	
	@JsonProperty("SHP_TYPE")
	private String shp_type;
	
	@JsonProperty("SCH_STS")
	private String sch_sts;
	
	@JsonProperty("LST_UPD")
	private String lst_upd;

}
