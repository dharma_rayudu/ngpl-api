package com.swire.ngpl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * @author Dharma Rayudu
 *
 */
@Data
public class BookingDetails {

	@JsonProperty("REL")
	private String rel;

	@JsonProperty("CRG_TYP")
	private String crg_typ;

	@JsonProperty("SHP_TYP")
	private String shp_typ;

	@JsonProperty("SCH_STS")
	private String sch_sts;

	@JsonProperty("LST_UPD")
	private String lst_upd;

	@JsonProperty("ACT_SEQ")
	private String act_seq;

	@JsonProperty("ACT_STS")
	private String act_sts;

	@JsonProperty("ACT_DES")
	private String act_des;

	@JsonProperty("ACT_DAT")
	private String act_dat;

	@JsonProperty("ACT_TIM")
	private String act_tim;

	@JsonProperty("ACT_LOC")
	private String act_loc;

	@JsonProperty("ACT_VVL")
	private String act_vvl;

	@JsonProperty("RFRN")
	private String rfrn;

	@JsonProperty("LACD")
	private String lacd;

	@JsonProperty("LACL")
	private String lacl;

	@JsonProperty("LACV")
	private String lacv;

	@JsonProperty("GAP")
	private String gap;

	@JsonProperty("ACT_FDT")
	private String act_fdt;

	@JsonProperty("BL_NO")
	private String bl_no;

	@JsonProperty("BK_NO")
	private String bk_no;

}
