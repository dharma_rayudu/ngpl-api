package com.swire.ngpl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Dharma Rayudu
 *
 */
@Getter
@Setter
public class ContainerActivities {

	@JsonProperty("ACT_SEQ")
	private String act_seq;

	@JsonProperty("ACT_STS")
	private String act_sts;

	@JsonProperty("ACT_DES")
	private String act_des;

	@JsonProperty("ACT_DAT")
	private String act_dat;

	@JsonProperty("ACT_TIM")
	private String act_tim;

	@JsonProperty("ACT_LOC")
	private String act_loc;

	@JsonProperty("ACT_VVL")
	private String act_vvl;

	@JsonProperty("LST_UPD")
	private String lst_upd;

}
