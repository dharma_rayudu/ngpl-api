package com.swire.ngpl.dao;

import java.util.List;

import com.swire.ngpl.model.LiveScheduleDetails;

/**
 * @author Dharma Rayudu
 *
 */
public interface LiveScheduleDao {

	public List<LiveScheduleDetails> getAllLiveScheduleDetails(String origin, String destination, String departureFrom,
			String departureTo, String arriveBy);

}
