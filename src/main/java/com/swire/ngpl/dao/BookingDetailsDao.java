package com.swire.ngpl.dao;

import java.util.List;

import com.swire.ngpl.model.BookingDetails;

/**
 * @author Dharma Rayudu
 *
 */
public interface BookingDetailsDao {

	public List<BookingDetails> getAllBookingDetails(String reference);

}
