package com.swire.ngpl.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.swire.ngpl.dao.BookingDetailsDao;
import com.swire.ngpl.mapper.BookingDetailsRowMapper;
import com.swire.ngpl.model.BookingDetails;

/**
 * @author Dharma Rayudu
 *
 */
@Repository
public class BookingDetailsDaoImpl implements BookingDetailsDao {

	private static final Logger log = LoggerFactory.getLogger(BookingDetailsDaoImpl.class);

	@Autowired
	private JdbcTemplate sqlserverJdbcTemplate;

	@Override
	public List<BookingDetails> getAllBookingDetails(String reference) {
		log.info("Inside getAllBookingDetails()");
		
		String queryForBookingDetails= "select " + 
				"rd.rd_ref, " + 
				"'0SUM' as rd_inf, " + 
				"'' as inf_seq, " + 
				"rd.sch_sts as sts_1, " + 
				"'' as dsc_1, " + 
				"'' as dat_1, " + 
				"'' as tim_1, " + 
				"'' as inf_1, " + 
				"'' as inf_2, " + 
				"rd.lst_upd as dts_1 " + 
				"from " + 
				"( " + 
				"select 0 as rd_cnt " + 
				") md " + 
				"inner join " + 
				"( " + 
				"select  " + 
				"'N/A' as rd_ref, " + 
				"case when GETDATE() < (select pp.sail_date as act_seq " + 
				"from ABSHEAD bh " + 
				"inner join TPLAN_REG pr on pr.vecode = bh.vescode and pr.voyage = bh.voyage " + 
				"inner join TPLAN_PORTS_REG pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage and pp.port = bh.poload " + 
				"where bh.absref='"+reference+"') then 'Planned'  " + 
				"when GETDATE() > (select pp.ARRIV_DATE " + 
				"from ABSHEAD bh " + 
				"inner join TPLAN_REG pr on pr.vecode = bh.vescode and pr.voyage = bh.voyage " + 
				"inner join TPLAN_PORTS_REG pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage and pp.port = bh.podis " + 
				"where bh.absref='"+reference+"') then 'Delivered'  " + 
				"else 'In Transit'  " + 
				"end as sch_sts, " + 
				"format(bh.edatetime, 'dd-MMM-yy hh:mm tt') as lst_upd " + 
				"from ABSHEAD bh " + 
				"where bh.absref='"+reference+"' " + 
				") rd " + 
				"on md.rd_cnt = md.rd_cnt " + 
				"where md.rd_cnt = 0 " + 
				"union all " + 
				"select " + 
				"rd.rd_ref, " + 
				"'1VVL' as rd_inf, " + 
				"format(rd.act_seq,'yyyy-MM-dd HH:mm') as inf_seq, " + 
				"rd.act_sts as sts_1, " + 
				"rd.act_des as dsc_1, " + 
				"rd.act_dat as dat_1, " + 
				"rd.act_tim as tim_1, " + 
				"rd.act_loc as inf_1, " + 
				"rd.act_vvl as inf_2, " + 
				"'' as dts_1 " + 
				"from " + 
				"( " + 
				"select 0 as rd_cnt " + 
				") md " + 
				"inner join " + 
				"( " + 
				"select " + 
				"'N/A' as rd_ref, " + 
				"pp.sail_date as act_seq, " + 
				"case when GETDATE() >= pp.sail_date then 'ACT' " + 
				"else 'EST' " + 
				"end as act_sts, " + 
				"'Load Onto Vessel' as act_des, " + 
				"format(pp.sail_date, 'dd MMM yyyy') as act_dat, " + 
				"format(pp.sail_date, 'hh:mm tt') as act_tim, " + 
				"concat(pn.descr,', ',pn.parreg) as act_loc, " + 
				"concat(pr.VECODE,pr.VOYAGE,' | ',vn.DESCR) as act_vvl " + 
				"from ABSHEAD bh " + 
				"inner join TPLAN_REG pr on pr.vecode = bh.vescode and pr.voyage = bh.voyage " + 
				"inner join TPLAN_PORTS_REG pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage and pp.port = bh.poload " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join VESSEL_REG vn on vn.VECODE = pr.VECODE " + 
				"where bh.absref='"+reference+"' " + 
				"union all " + 
				"select " + 
				"'N/A' as rd_ref, " + 
				"pp.arriv_date as act_seq, " + 
				"case when GETDATE() >= pp.arriv_date then 'ACT' " + 
				"else 'EST' " + 
				"end as act_sts, " + 
				"'Unload From Vessel' as act_des, " + 
				"format(pp.arriv_date, 'dd MMM yyyy') as act_dat, " + 
				"format(pp.arriv_date, 'hh:mm tt') as act_tim, " + 
				"concat(pn.descr,', ',pn.parreg) as act_loc, " + 
				"concat(pr.VECODE,pr.VOYAGE,' | ',vn.DESCR) as act_vvl " + 
				"from ABSHEAD bh " + 
				"inner join TPLAN_REG pr on pr.vecode = bh.vescode and pr.voyage = bh.voyage " + 
				"inner join TPLAN_PORTS_REG pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage and pp.port = bh.podis " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join VESSEL_REG vn on vn.VECODE = pr.VECODE " + 
				"where bh.absref='"+reference+"' " + 
				") rd " + 
				"on md.rd_cnt = md.rd_cnt " + 
				"where md.rd_cnt = 0 " + 
				"union all " + 
				"select " + 
				"rd.rd_ref, " + 
				"'2CNL' as rd_inf, " + 
				"convert(varchar,rd.rfrn) as inf_seq, " + 
				"concat(rd.gap,'') as sts_1, " + 
				"rd.lacd as dsc_1, " + 
				"'' as dat_1, " + 
				"'' as tim_1, " + 
				"rd.lacl as inf_1, " + 
				"rd.lacv as inf_2, " + 
				"'' as dts_1 " + 
				"from " + 
				"( " + 
				"select 0 as rd_cnt " + 
				") md " + 
				"inner join " + 
				"( " + 
				"select " + 
				"'N/A' as rd_ref, " + 
				"ctr.eqnum as rfrn, " + 
				"ca.descr as lacd, " + 
				"concat(ar.descr,', ',ar.parreg) as lacl, " + 
				"case when ca.activity = 'LOAD' then concat(ch.vessel,ch.voyage,' | ',vn.descr) when ca.activity = 'DISC' then concat(ch.vessel,ch.voyage,' | ',vn.descr) else '' end as lacv, " + 
				"sum (case when ctr_act.activity is null then 1 else 0 end) as gap " + 
				"from " + 
				"( " + 
				"select  " + 
				"bh.absref, " + 
				"bp.eqnum " + 
				"from abshead bh  " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"where bh.absref = '"+reference+"' " + 
				") ctr " + 
				"left outer join  " + 
				"( " + 
				"select  " + 
				"distinct " + 
				"ca.activity, " + 
				"case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end as actreg " + 
				"from abshead bh  " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"inner join eqhist ch on ch.eqnum = bp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where bh.absref = '"+reference+"' " + 
				") act on 1=1 " + 
				"left outer join  " + 
				"( " + 
				"select  " + 
				"bp.eqnum, " + 
				"ca.activity, " + 
				"case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end as actreg " + 
				"from abshead bh  " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"inner join eqhist ch on ch.eqnum = bp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where bh.absref = '"+reference+"' " + 
				") ctr_act on ctr_act.eqnum = ctr.eqnum and ctr_act.activity = act.activity and ctr_act.actreg = act.actreg " + 
				"left outer join " + 
				"( " + 
				"select  " + 
				"bp.eqnum, " + 
				"max(ch.eqhistid) as eqhistid " + 
				"from abshead bh  " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"inner join eqhist ch on ch.eqnum = bp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where bh.absref = '"+reference+"' " + 
				"group by " + 
				"bp.eqnum " + 
				") act_lst on act_lst.eqnum = ctr.eqnum " + 
				"left outer join eqhist ch on ch.eqhistid = act_lst.eqhistid " + 
				"left outer join activity_reg ca on ca.activity = ch.activity " + 
				"left outer join vessel_reg vn on vn.vecode = (case when ca.activity = 'LOAD' then ch.vessel when ca.activity = 'DISC' then ch.vessel else '' end) " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"group by " + 
				"ctr.eqnum, " + 
				"ca.descr, " + 
				"concat(ar.descr,', ',ar.parreg), " + 
				"case when ca.activity = 'LOAD' then concat(ch.vessel,ch.voyage,' | ',vn.descr) when ca.activity = 'DISC' then concat(ch.vessel,ch.voyage,' | ',vn.descr) else '' end " + 
				") rd " + 
				"on md.rd_cnt = md.rd_cnt " + 
				"where md.rd_cnt = 0 " + 
				"order by rd_ref, rd_inf, inf_seq; " + 
				"";

		log.info("Query for Booking Details:-" + queryForBookingDetails);

		RowMapper<BookingDetails> rowMapper = new BookingDetailsRowMapper();
		return sqlserverJdbcTemplate.query(queryForBookingDetails, rowMapper);

	}

}
