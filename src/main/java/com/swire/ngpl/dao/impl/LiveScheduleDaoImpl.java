package com.swire.ngpl.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.swire.ngpl.dao.LiveScheduleDao;
import com.swire.ngpl.mapper.LiveScheduleRowMapper;
import com.swire.ngpl.model.LiveScheduleDetails;

/**
 * @author Dharma Rayudu
 *
 */
@Repository
public class LiveScheduleDaoImpl implements LiveScheduleDao {
	private static final Logger log = LoggerFactory.getLogger(LiveScheduleDaoImpl.class);

	@Autowired
	private JdbcTemplate sqlserverJdbcTemplate;

	@Override
	public List<LiveScheduleDetails> getAllLiveScheduleDetails(String origin, String destination, String departureFrom,
			String departureTo, String arriveBy) {
		log.info("Inside getAllLiveScheduleDetails()");
		
		String queryForLiveSchedule = "select " + 
				"pp_ld.port as pld, " + 
				"pp_ld.descr as pld_nam, " + 
				"pp_ld.cndes as pld_cnm, " + 
				"pp_dc.port as pdc, " + 
				"pp_dc.descr as pdc_nam, " + 
				"pp_dc.cndes as pdc_cnm, " + 
				"pp_ld.sail_date as etd, " + 
				"pp_dc.arriv_date as eta, " + 
				"DATEDIFF(day,pp_ld.sail_date,pp_dc.arriv_date )+1 as trt_tim, " + 
				"tp.tpcode as srv, " + 
				"tp.tpdescr as srv_nam, " + 
				"pp_ld.vecode as vsl, " + 
				"pp_ld.voyage as vyg, " + 
				"vr.descr as vsl_nam " + 
				"from " + 
				"( " + 
				"select " + 
				"pr.vecode, " + 
				"pr.voyage, " + 
				"pr.prcode, " + 
				"pr.tpcode, " + 
				"pp.port, " + 
				"pn.descr, " + 
				"cn.descr as cndes, " + 
				"pp.sail_date " + 
				"from tplan_reg pr " + 
				"inner join tplan_ports_reg pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join region_reg cn on cn.regcode = pn.parreg " + 
				"where pr.prcode = 'NGPL' " + 
				"and pp.sail_date is not null " + 
				"and pp.lport = 'Y' " + 
				"and pp.port like concat('"+origin+"','%') " + 
				"and pp.sail_date between '"+departureFrom+"' and '"+departureTo+"' " + 
				") pp_ld " + 
				" inner join " + 
				"( " + 
				"select " + 
				"pr.vecode, " + 
				"pr.voyage, " + 
				"pr.prcode, " + 
				"pr.tpcode, " + 
				"pp.port, " + 
				"pn.descr, " + 
				"cn.descr as cndes, " + 
				"pp.arriv_date " + 
				"from tplan_reg pr " + 
				"inner join tplan_ports_reg pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join region_reg cn on cn.regcode = pn.parreg " + 
				"where pr.prcode = 'NGPL' " + 
				"and pp.arriv_date is not null " + 
				"and pp.dport = 'Y' " + 
				"and pp.port like concat('"+destination+"','%') " + 
				"and pp.arriv_date <= '"+arriveBy+"' " + 
				") pp_dc on pp_dc.vecode = pp_ld.vecode and pp_dc.voyage = pp_ld.voyage and pp_dc.arriv_date >= pp_ld.sail_date " + 
				"inner join tpath_reg tp on tp.tpcode = pp_ld.tpcode and tp.prcode = pp_ld.prcode " + 
				"inner join vessel_reg vr on vr.vecode = pp_ld.vecode " + 
				" order by pp_ld.port, pp_dc.port, pp_ld.sail_date " + 
				"";

		log.info("Query For LiveSchedule:-" + queryForLiveSchedule);

		RowMapper<LiveScheduleDetails> rowMapper = new LiveScheduleRowMapper();

		return sqlserverJdbcTemplate.query(queryForLiveSchedule, rowMapper);
	}

}
