package com.swire.ngpl.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.swire.ngpl.dao.ProductDetailsDao;
import com.swire.ngpl.mapper.ProductDetailsRowMapper;
import com.swire.ngpl.model.ProductDetails;

/**
 * @author Dharma Rayudu
 *
 */
@Repository
public class ProductDetailsDaoImpl implements ProductDetailsDao {
	private static final Logger log = LoggerFactory.getLogger(ProductDetailsDaoImpl.class);

	@Autowired
	private JdbcTemplate sqlserverJdbcTemplate;

	@Override
	public List<ProductDetails> getAllProductDetails(String destination, String origin) {
		log.info("Inside getAllProductDetails()");
		
		String queryForProductDetails = "select " + 
				"sv_sh_rf.pld, " + 
				"sv_sh_rf.pld_nam, " + 
				"sv_sh_rf.pld_cnm, " + 
				"sv_sh_rf.pdc, " + 
				"sv_sh_rf.pdc_nam, " + 
				"sv_sh_rf.pdc_cnm, " + 
				"sv_sh_rf.srv, " + 
				"sv_sh_rf.srv_nam, " + 
				"avg(sv_sh_rf.trt_tim) as trt_tim, " + 
				"min(sv_sh_rf.etd) as etd, " + 
				"avg(datediff(day,sv_sh_rf.etd,coalesce(sv_sh_pv.etd,sv_sh_rf.etd))) as frq " + 
				"from " + 
				"( " + 
				"select " + 
				"row_number() over (order by pp_ld.descr, pp_dc.descr, tp.tpdescr, pp_ld.sail_date) as rno, " + 
				"pp_ld.port as pld, " + 
				"pp_ld.descr as pld_nam, " + 
				"pp_ld.cndes as pld_cnm, " + 
				"pp_dc.port as pdc, " + 
				"pp_dc.descr as pdc_nam, " + 
				"pp_dc.cndes as pdc_cnm, " + 
				"pp_ld.sail_date as etd, " + 
				"pp_dc.arriv_date as eta, " + 
				"DATEDIFF(day,pp_ld.sail_date,pp_dc.arriv_date )+1 as trt_tim, " + 
				"tp.tpcode as srv, " + 
				"tp.tpdescr as srv_nam, " + 
				"pp_ld.vecode as vsl, " + 
				"pp_ld.voyage as vyg, " + 
				"vr.descr as vsl_nam " + 
				"from " + 
				"( " + 
				"select " + 
				"pr.vecode, " + 
				"pr.voyage, " + 
				"pr.prcode, " + 
				"pr.tpcode, " + 
				"pp.port, " + 
				"pn.descr, " + 
				"cn.descr as cndes, " + 
				"pp.sail_date " + 
				"from tplan_reg pr " + 
				"inner join tplan_ports_reg pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join region_reg cn on cn.regcode = pn.parreg " + 
				"where pr.prcode = 'NGPL' " + 
				"and pp.sail_date is not null " + 
				"and pp.lport = 'Y' " + 
				"and pp.port like concat('"+origin+"','%') " + 
				"and pp.sail_date between '2020-01-01' and '2020-06-30' " + 
				") pp_ld " + 
				"inner join " + 
				"( " + 
				"select " + 
				"pr.vecode, " + 
				"pr.voyage, " + 
				"pr.prcode, " + 
				"pr.tpcode, " + 
				"pp.port, " + 
				"pn.descr, " + 
				"cn.descr as cndes, " + 
				"pp.arriv_date " + 
				"from tplan_reg pr " + 
				"inner join tplan_ports_reg pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join region_reg cn on cn.regcode = pn.parreg " + 
				"where pr.prcode = 'NGPL' " + 
				"and pp.arriv_date is not null " + 
				"and pp.dport = 'Y' " + 
				"and pp.port like concat('"+destination+"','%') " + 
				"and pp.arriv_date <= '2020-08-31' " + 
				") pp_dc on pp_dc.vecode = pp_ld.vecode and pp_dc.voyage = pp_ld.voyage and pp_dc.arriv_date >= pp_ld.sail_date " + 
				"inner join tpath_reg tp on tp.tpcode = pp_ld.tpcode and tp.prcode = pp_ld.prcode " + 
				"inner join vessel_reg vr on vr.vecode = pp_ld.vecode " + 
				") sv_sh_rf " + 
				"left outer join  " + 
				"( " + 
				"select " + 
				"row_number() over (order by pp_ld.descr, pp_dc.descr, tp.tpdescr, pp_ld.sail_date) as rno, " + 
				"pp_ld.port as pld, " + 
				"pp_ld.descr as pld_nam, " + 
				"pp_ld.cndes as pld_cnm, " + 
				"pp_dc.port as pdc, " + 
				"pp_dc.descr as pdc_nam, " + 
				"pp_dc.cndes as pdc_cnm, " + 
				"pp_ld.sail_date as etd, " + 
				"pp_dc.arriv_date as eta, " + 
				"DATEDIFF(day,pp_ld.sail_date,pp_dc.arriv_date )+1 as trt_tim, " + 
				"tp.tpcode as srv, " + 
				"tp.tpdescr as srv_nam, " + 
				"pp_ld.vecode as vsl, " + 
				"pp_ld.voyage as vyg, " + 
				"vr.descr as vsl_nam " + 
				"from " + 
				"( " + 
				"select " + 
				"pr.vecode, " + 
				"pr.voyage, " + 
				"pr.prcode, " + 
				"pr.tpcode, " + 
				"pp.port, " + 
				"pn.descr, " + 
				"cn.descr as cndes, " + 
				"pp.sail_date " + 
				"from tplan_reg pr " + 
				"inner join tplan_ports_reg pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join region_reg cn on cn.regcode = pn.parreg " + 
				"where pr.prcode = 'NGPL' " + 
				"and pp.sail_date is not null " + 
				"and pp.lport = 'Y' " + 
				"and pp.port like concat('"+origin+"','%') " + 
				"and pp.sail_date between '2020-01-01' and '2020-06-30' " + 
				") pp_ld " + 
				"inner join " + 
				"( " + 
				"select " + 
				"pr.vecode, " + 
				"pr.voyage, " + 
				"pr.prcode, " + 
				"pr.tpcode, " + 
				"pp.port, " + 
				"pn.descr, " + 
				"cn.descr as cndes, " + 
				"pp.arriv_date " + 
				"from tplan_reg pr " + 
				"inner join tplan_ports_reg pp on pp.vecode  = pr.vecode and pp.voyage =  pr.voyage " + 
				"inner join region_reg pn on pn.regcode = pp.port " + 
				"inner join region_reg cn on cn.regcode = pn.parreg " + 
				"where pr.prcode = 'NGPL' " + 
				"and pp.arriv_date is not null " + 
				"and pp.dport = 'Y' " + 
				"and pp.port like concat('"+destination+"','%') " + 
				"and pp.arriv_date <= '2020-08-31' " + 
				") pp_dc on pp_dc.vecode = pp_ld.vecode and pp_dc.voyage = pp_ld.voyage and pp_dc.arriv_date >= pp_ld.sail_date " + 
				"inner join tpath_reg tp on tp.tpcode = pp_ld.tpcode and tp.prcode = pp_ld.prcode " + 
				"inner join vessel_reg vr on vr.vecode = pp_ld.vecode " + 
				") sv_sh_pv on sv_sh_pv.pld = sv_sh_rf.pld and sv_sh_pv.pdc = sv_sh_rf.pdc and sv_sh_pv.srv = sv_sh_rf.srv and sv_sh_pv.rno = (sv_sh_rf.rno + 1) " + 
				"group by " + 
				"sv_sh_rf.pld, " + 
				"sv_sh_rf.pld_nam, " + 
				"sv_sh_rf.pld_cnm, " + 
				"sv_sh_rf.pdc, " + 
				"sv_sh_rf.pdc_nam, " + 
				"sv_sh_rf.pdc_cnm, " + 
				"sv_sh_rf.srv, " + 
				"sv_sh_rf.srv_nam " + 
				"order by pld_nam, pdc_nam, etd " + 
				"";

		log.info("Query for Product details:- " + queryForProductDetails);

		RowMapper<ProductDetails> rowMapper = new ProductDetailsRowMapper();
		return sqlserverJdbcTemplate.query(queryForProductDetails, rowMapper);
	}

	@Override
	public long countNoOfServices(String pld) {
		String queryToCountNoOfservices = "SELECT COUNT('" + pld + "') from test";

		return sqlserverJdbcTemplate.queryForObject(queryToCountNoOfservices, Long.class);
	}

}
