package com.swire.ngpl.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.swire.ngpl.dao.NgplTrackAndTraceDao;
import com.swire.ngpl.mapper.ActivitiesReferenceMapper;
import com.swire.ngpl.mapper.ContainerActivitiesMapper;
import com.swire.ngpl.model.ActivitiesReferenceList;
import com.swire.ngpl.model.ContainerActivities;

/**
 * @author Dharma Rayudu
 *
 */
@Repository
public class NgplTrackAndTraceDaoImpl implements NgplTrackAndTraceDao {

	private static final Logger log = LoggerFactory.getLogger(NgplTrackAndTraceDaoImpl.class);

	@Autowired
	private JdbcTemplate sqlserverJdbcTemplate;

	@Override
	public List<ContainerActivities> getContainerTrackingDetails(String reference, String bkno, String blno) {
		log.info("Inside getContainerTrackingDetails()");
		
		String queryForContainerDetails = "select " + 
				"concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm')) as inf_seq, " + 
				"case when getdate() > convert(datetime, concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm:ss')),120) then 'ACT' " + 
				"else 'EST' " + 
				"end as sts_1, " + 
				"ca.descr as dsc_1, " + 
				"format(ch.actdate, 'dd MMM yyyy') as dat_1, " + 
				"format(ch.acttime, 'hh:mm tt') as tim_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1, " + 
				"case when ca.activity = 'LOAD' then concat(ch.vessel,ch.voyage,' | ',vn.descr) when ca.activity = 'DISC' then concat(ch.vessel,ch.voyage,' | ',vn.descr) else '' end as inf_2, " + 
				"format(ch.entry_d_time, 'dd-MMM-yy hh:mm tt') as dts_1 " + 
				"from " + 
				"abshead bh " + 
				"inner join eqhist ch on ch.eqnum = '"+reference+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join vessel_reg vn on vn.vecode = (case when ca.activity = 'LOAD' then ch.vessel when ca.activity = 'DISC' then ch.vessel else '' end) " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"where bh.absref = '"+bkno+"' " + 
				"union all " + 
				"select " + 
				"concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm')) as inf_seq, " + 
				"case when getdate() > convert(datetime, concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm:ss')),120) then 'ACT' " + 
				"else 'EST' " + 
				"end as sts_1, " + 
				"ca.descr as dsc_1, " + 
				"format(ch.actdate, 'dd MMM yyyy') as dat_1, " + 
				"format(ch.acttime, 'hh:mm tt') as tim_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1, " + 
				"case when ca.activity = 'LOAD' then concat(ch.vessel,ch.voyage,' | ',vn.descr) when ca.activity = 'DISC' then concat(ch.vessel,ch.voyage,' | ',vn.descr) else '' end as inf_2, " + 
				"format(ch.entry_d_time, 'dd-MMM-yy hh:mm tt') as dts_1 " + 
				"from " + 
				"adshead dh " + 
				"inner join eqhist ch on ch.eqnum = '"+reference+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join vessel_reg vn on vn.vecode = (case when ca.activity = 'LOAD' then ch.vessel when ca.activity = 'DISC' then ch.vessel else '' end) " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"where dh.refnum = '"+blno+"' " + 
				"order by inf_seq " + 
				"";

		log.info("Container Activities Query:- " + queryForContainerDetails);
		RowMapper<ContainerActivities> rowMapper = new ContainerActivitiesMapper();

		return sqlserverJdbcTemplate.query(queryForContainerDetails, rowMapper);
	}

	@Override
	public List<ActivitiesReferenceList> getActivitiesTrackingDetails(String reference, String bkno, String blno) {
		log.info("Inside getActivitiesTrackingDetails()");
		
		String queryForActivitiesReference = "select " + 
				"min(concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm'))) as inf_seq, " + 
				"ca.descr as dsc_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1 " + 
				"from " + 
				"abshead bh " + 
				"inner join eqhist ch on ch.eqnum = '"+reference+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"where bh.absref = '"+bkno+"' " + 
				"group by " + 
				"ca.descr, " + 
				"concat(ar.descr,', ',ar.parreg) " + 
				"union all " + 
				"select " + 
				"min(concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm'))) as inf_seq, " + 
				"ca.descr as dsc_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1 " + 
				"from " + 
				"adshead dh " + 
				"inner join eqhist ch on ch.eqnum = '"+reference+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"where dh.refnum = '"+blno+"' " + 
				"group by " + 
				"ca.descr, " + 
				"concat(ar.descr,', ',ar.parreg) " + 
				"order by inf_seq; " + 
				" " + 
				"";

		log.info("Query for activites reference:-" + queryForActivitiesReference);

		RowMapper<ActivitiesReferenceList> rowMapper = new ActivitiesReferenceMapper();
		return sqlserverJdbcTemplate.query(queryForActivitiesReference, rowMapper);
	}

}
