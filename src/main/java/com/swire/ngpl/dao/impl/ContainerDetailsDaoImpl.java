package com.swire.ngpl.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.swire.ngpl.dao.ContainerDetailsDao;
import com.swire.ngpl.mapper.ContainerDetailsRowMapper;
import com.swire.ngpl.model.BookingDetails;

/**
 * @author Dharma Rayudu
 *
 */
@Repository
public class ContainerDetailsDaoImpl implements ContainerDetailsDao {

	private static final Logger log = LoggerFactory.getLogger(ContainerDetailsDaoImpl.class);

	@Autowired
	private JdbcTemplate sqlserverJdbcTemplate;

	@Override
	public List<BookingDetails> getAllContainerDetails(String containerNumber) {
		log.info("Inside getAllContainerDetails()");

		
		String queryForContainer = "select " + 
				"min(ch.eqhistid) as rd_ref, " + 
				"'N/A' as bl_no, " + 
				"bh.absref as bk_no, " + 
				"'0SUM' as rd_inf, " + 
				"'' as inf_seq, " + 
				"case when GETDATE() < lpp.sail_date then 'Planned' " + 
				"when GETDATE() > dpp.arriv_date then 'Delivered' " + 
				"else 'In Transit' " + 
				"end as sts_1, " + 
				"'' as dsc_1, " + 
				"'' as dat_1, " + 
				"'' as tim_1, " + 
				"'' as inf_1, " + 
				"'' as inf_2, " + 
				"format(bh.edatetime, 'dd-MMM-yy hh:mm tt') as dts_1 " + 
				"from abshead bh " + 
				"inner join TPLAN_REG lpr on lpr.vecode = bh.vescode and lpr.voyage = bh.voyage " + 
				"inner join TPLAN_PORTS_REG lpp on lpp.vecode  = lpr.vecode and lpp.voyage =  lpr.voyage and lpp.port = bh.poload " + 
				"inner join TPLAN_REG dpr on dpr.vecode = bh.vescode and dpr.voyage = bh.voyage " + 
				"inner join TPLAN_PORTS_REG dpp on dpp.vecode  = dpr.vecode and dpp.voyage =  dpr.voyage and dpp.port = bh.podis " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"inner join eqhist ch on ch.eqnum = bp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where bp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				"and bh.absref not in " + 
				"( " + 
				"select dh.absref " + 
				"from adshead dh " + 
				"inner join adspack dp on dp.refnum = dh.refnum " + 
				"inner join eqhist ch on ch.eqnum = dp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where dp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				") " + 
				"group by " + 
				"bh.absref, " + 
				"case when GETDATE() < lpp.sail_date then 'Planned' " + 
				"when GETDATE() > dpp.arriv_date then 'Delivered' " + 
				"else 'In Transit' " + 
				"end, " + 
				"format(bh.edatetime, 'dd-MMM-yy hh:mm tt') " + 
				"union all " + 
				"select " + 
				"min(ch.eqhistid) as rd_ref, " + 
				"dh.refnum as bl_no, " + 
				"'N/A' as bk_no, " + 
				"'0SUM' as rd_inf, " + 
				"'' as inf_seq, " + 
				"case when GETDATE() < lpp.sail_date then 'Planned' " + 
				"when GETDATE() > dpp.arriv_date then 'Delivered' " + 
				"else 'In Transit' " + 
				"end as sts_1, " + 
				"'' as dsc_1, " + 
				"'' as dat_1, " + 
				"'' as tim_1, " + 
				"'' as inf_1, " + 
				"'' as inf_2, " + 
				"format(dh.edatetime, 'dd-MMM-yy hh:mm tt') as dts_1 " + 
				"from adshead dh " + 
				"inner join TPLAN_REG lpr on lpr.vecode = dh.vessel and lpr.voyage = dh.voyage " + 
				"inner join TPLAN_PORTS_REG lpp on lpp.vecode  = lpr.vecode and lpp.voyage =  lpr.voyage and lpp.port = dh.poload " + 
				"inner join TPLAN_REG dpr on dpr.vecode = dh.vessel and dpr.voyage = dh.voyage " + 
				"inner join TPLAN_PORTS_REG dpp on dpp.vecode  = dpr.vecode and dpp.voyage =  dpr.voyage and dpp.port = dh.podis " + 
				"inner join adspack dp on dp.refnum = dh.refnum " + 
				"inner join eqhist ch on ch.eqnum = dp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where dp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				"group by " + 
				"dh.refnum, " + 
				"case when GETDATE() < lpp.sail_date then 'Planned' " + 
				"when GETDATE() > dpp.arriv_date then 'Delivered' " + 
				"else 'In Transit' " + 
				"end, " + 
				"format(dh.edatetime, 'dd-MMM-yy hh:mm tt') " + 
				"union all " + 
				"select " + 
				"md.rd_ref as rd_ref, " + 
				"'N/A' as bl_no, " + 
				"md.bk_no as bk_no, " + 
				"'1CAC' as rd_inf, " + 
				"concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm')) as inf_seq, " + 
				"case when getdate() > convert(datetime, concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm:ss')),120) then 'ACT' " + 
				"else 'EST' " + 
				"end as sts_1, " + 
				"ca.descr as dsc_1, " + 
				"format(ch.actdate, 'dd MMM yyyy') as dat_1, " + 
				"format(ch.acttime, 'hh:mm tt') as tim_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1, " + 
				"case when ca.activity = 'LOAD' then concat(ch.vessel,ch.voyage,' | ',vn.descr) when ca.activity = 'DISC' then concat(ch.vessel,ch.voyage,' | ',vn.descr) else '' end as inf_2, " + 
				"format(ch.entry_d_time, 'dd-MMM-yy hh:mm tt') as dts_1 " + 
				"from " + 
				"( " + 
				"select " + 
				"min(ch.eqhistid) as rd_ref, " + 
				"'N/A' as bl_no, " + 
				"bh.absref as bk_no " + 
				"from abshead bh " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"inner join eqhist ch on ch.eqnum = bp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where bp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				"and bh.absref not in " + 
				"( " + 
				"select dh.absref " + 
				"from adshead dh " + 
				"inner join adspack dp on dp.refnum = dh.refnum " + 
				"inner join eqhist ch on ch.eqnum = dp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where dp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				") " + 
				"group by " + 
				"bh.absref " + 
				") md " + 
				"inner join abshead bh on bh.absref = md.bk_no " + 
				"inner join eqhist ch on ch.eqnum = '"+containerNumber+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join vessel_reg vn on vn.vecode = (case when ca.activity = 'LOAD' then ch.vessel when ca.activity = 'DISC' then ch.vessel else '' end) " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"union all " + 
				"select " + 
				"md.rd_ref as rd_ref, " + 
				"md.bl_no as bl_no, " + 
				"'N/A' as bk_no, " + 
				"'1CAC' as rd_inf, " + 
				"concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm')) as inf_seq, " + 
				"case when getdate() > convert(datetime, concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm:ss')),120) then 'ACT' " + 
				"else 'EST' " + 
				"end as sts_1, " + 
				"ca.descr as dsc_1, " + 
				"format(ch.actdate, 'dd MMM yyyy') as dat_1, " + 
				"format(ch.acttime, 'hh:mm tt') as tim_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1, " + 
				"case when ca.activity = 'LOAD' then concat(ch.vessel,ch.voyage,' | ',vn.descr) when ca.activity = 'DISC' then concat(ch.vessel,ch.voyage,' | ',vn.descr) else '' end as inf_2, " + 
				"format(ch.entry_d_time, 'dd-MMM-yy hh:mm tt') as dts_1 " + 
				"from " + 
				"( " + 
				"select " + 
				"min(ch.eqhistid) as rd_ref, " + 
				"dh.refnum as bl_no, " + 
				"'N/A' as bk_no " + 
				"from adshead dh " + 
				"inner join adspack dp on dp.refnum = dh.refnum " + 
				"inner join eqhist ch on ch.eqnum = dp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where dp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				"group by " + 
				"dh.refnum " + 
				") md " + 
				"inner join adshead dh on dh.refnum = md.bl_no " + 
				"inner join eqhist ch on ch.eqnum = '"+containerNumber+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join vessel_reg vn on vn.vecode = (case when ca.activity = 'LOAD' then ch.vessel when ca.activity = 'DISC' then ch.vessel else '' end) " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"union all " + 
				"select " + 
				"md.rd_ref as rd_ref, " + 
				"md.bl_no as bl_no, " + 
				"md.bk_no as bk_no, " + 
				"'2ARL' as rd_inf, " + 
				"min(concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm'))) as inf_seq, " + 
				"'' as sts_1, " + 
				"ca.descr as dsc_1, " + 
				"'' as dat_1, " + 
				"'' as tim_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1, " + 
				"'' as inf_2, " + 
				"'' as dts_1 " + 
				"from " + 
				"( " + 
				"select " + 
				"min(ch.eqhistid) as rd_ref, " + 
				"'N/A' as bl_no, " + 
				"bh.absref as bk_no " + 
				"from abshead bh " + 
				"inner join abspack bp on bp.absref = bh.absref " + 
				"inner join eqhist ch on ch.eqnum = bp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where bp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				"and bh.absref not in " + 
				"( " + 
				"select dh.absref " + 
				"from adshead dh " + 
				"inner join adspack dp on dp.refnum = dh.refnum " + 
				"inner join eqhist ch on ch.eqnum = dp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where dp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				") " + 
				"group by " + 
				"bh.absref " + 
				") md " + 
				"inner join abshead bh on bh.absref = md.bk_no " + 
				"inner join eqhist ch on ch.eqnum = '"+containerNumber+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(bh.vescode,bh.voyage) or concat(ch.vessel,ch.voyage) = concat(bh.precrves,bh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(bh.oncrves,bh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"group by " + 
				"md.rd_ref, " + 
				"md.bl_no, " + 
				"md.bk_no, " + 
				"ca.descr, " + 
				"concat(ar.descr,', ',ar.parreg) " + 
				"union all " + 
				"select " + 
				"md.rd_ref as rd_ref, " + 
				"md.bl_no as bl_no, " + 
				"md.bk_no as bk_no, " + 
				"'2ARL' as rd_inf, " + 
				"min(concat(format(ch.actdate,'yyyy-MM-dd'),' ',format(ch.acttime,'HH:mm'))) as inf_seq, " + 
				"'' as sts_1, " + 
				"ca.descr as dsc_1, " + 
				"'' as dat_1, " + 
				"'' as tim_1, " + 
				"concat(ar.descr,', ',ar.parreg) as inf_1, " + 
				"'' as inf_2, " + 
				"'' as dts_1 " + 
				"from " + 
				"( " + 
				"select " + 
				"min(ch.eqhistid) as rd_ref, " + 
				"dh.refnum as bl_no, " + 
				"'N/A' as bk_no " + 
				"from adshead dh " + 
				"inner join adspack dp on dp.refnum = dh.refnum " + 
				"inner join eqhist ch on ch.eqnum = dp.eqnum and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"inner join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"where dp.eqnum = '"+containerNumber+"' " + 
				"and ch.actdate between dateadd(d,-180,getdate()) and dateadd(d,30,getdate()) " + 
				"group by " + 
				"dh.refnum " + 
				") md " + 
				"inner join adshead dh on dh.refnum = md.bl_no " + 
				"inner join eqhist ch on ch.eqnum = '"+containerNumber+"' and concat(ch.vessel,ch.voyage) <> '' and (concat(ch.vessel,ch.voyage) = concat(dh.vessel,dh.voyage) or concat(ch.vessel,ch.voyage) = concat(dh.precrves,dh.precrvoy) or concat(ch.vessel,ch.voyage) = concat(dh.oncrves,dh.oncrvoy)) " + 
				"left outer join activity_reg ca on ca.activity = ch.activity and (ca.mode = 'L' or ca.mode = 'S' or ca.mode = 'T') " + 
				"left outer join region_reg ar on ar.regcode = (case when ca.activity = 'LOAD' then ch.loadreg when ca.activity = 'DISC' then ch.disreg else ch.actreg end) " + 
				"group by " + 
				"md.rd_ref, " + 
				"md.bl_no, " + 
				"md.bk_no, " + 
				"ca.descr, " + 
				"concat(ar.descr,', ',ar.parreg) " + 
				"order by bl_no, rd_ref, rd_inf, inf_seq; " + 
				"";

		log.info("Query for Container:-" + queryForContainer);

		RowMapper<BookingDetails> rowMapper = new ContainerDetailsRowMapper();
		return sqlserverJdbcTemplate.query(queryForContainer, rowMapper);
	}

}
