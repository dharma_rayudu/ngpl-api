package com.swire.ngpl.dao;

import java.util.List;

import com.swire.ngpl.model.BookingDetails;

public interface ContainerDetailsDao {

	public List<BookingDetails> getAllContainerDetails(String containerNumber);

}
