package com.swire.ngpl.dao;

import java.util.List;

import com.swire.ngpl.model.ProductDetails;

public interface ProductDetailsDao {

	public List<ProductDetails> getAllProductDetails(String destination, String origin);
	public long countNoOfServices(String pld);

}
