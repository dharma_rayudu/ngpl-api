package com.swire.ngpl.dao;

import java.util.List;

import com.swire.ngpl.model.ActivitiesReferenceList;
import com.swire.ngpl.model.ContainerActivities;

/**
 * @author Dharma Rayudu
 *
 */
public interface NgplTrackAndTraceDao {

	public List<ContainerActivities> getContainerTrackingDetails(String reference, String bkno, String blno);

	public List<ActivitiesReferenceList> getActivitiesTrackingDetails(String reference, String bkno, String blno);
}
