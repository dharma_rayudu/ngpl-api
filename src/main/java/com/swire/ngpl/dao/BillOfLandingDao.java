package com.swire.ngpl.dao;

import java.util.List;

import com.swire.ngpl.model.BookingDetails;

public interface BillOfLandingDao {

	public List<BookingDetails> getAllForBl(String blNumber);

}
